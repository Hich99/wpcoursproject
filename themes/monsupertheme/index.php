<?php get_header(); ?>
    <section class="row">
        <?php if (is_active_sidebar('macentral')) :
            dynamic_sidebar('macentral');
        endif; ?>
    </section>
    <main class="row">
        <section class="blog-main col-sm-8">
            <?php
                // si il y a des posts on les parcours
                if ( have_posts() ) : while ( have_posts() ) : the_post(); 
                        // on passe l'objet du post au template part content
                        get_template_part('content', get_post_format());
                    endwhile;
                endif;
            ?>
        </section>
        <?php get_sidebar(); ?>
    </main>
<?php get_footer(); ?>
  