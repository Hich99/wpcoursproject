<?php get_header(); ?>
	<main class="row">
		<header>
			<?php
				the_archive_title("<h3 class='entry-title'>", "</h3>");
				the_archive_description("<em class='taxonomy-description'>","</em>");
			?>
		</header>
		<section class="blog-main">
			<?php
			// si il y a des posts on les parcours
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				// on passe l'objet du post au template part content-catégory
				get_template_part('content', 'category');
			endwhile;
			endif;
			?>
		</section>
	</main>
<?php get_footer(); ?>
