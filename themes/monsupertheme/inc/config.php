<?php

/**
	fichier de config de notre theme
 */

function monsupertheme_setup(){
	global $content_width;
	if( !isset( $content_width ) ) {
		$content_width = 1650; // pixel
	}

	/**
	 * ajout le support de flux rss dans l'entête
	 */
	add_theme_support('automatic-feed-links');

	add_theme_support('post-thumbnails');

	$args = [
		"default-image" => get_template_directory_uri()
		                   . "/img/default.jpg",
		"default-text-color" => '000',
		"width" => '100%',
		"height" => 350,
		'flex-width' => true,
		"flex-height" => true
	];

	add_theme_support('custom-header', $args);

}
// ajout du hook de setup
add_action('after_setup_theme','monsupertheme_setup');
