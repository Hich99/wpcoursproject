<?php


// fonction de mise en queue des scripts
function monsupertheme_scripts()
{
	wp_enqueue_style('bootstrap',
		'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css',
		[],'1.0');
	wp_enqueue_style("main",
		get_template_directory_uri() . "/css/main.css",
		[], '1.0');
	wp_enqueue_script('jqueryjs',
		'https://code.jquery.com/jquery-3.6.0.min.js',
		[], '3.6.0', true);
	wp_enqueue_script('popperjs',
		'https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.2/umd/popper.min.js',
		[], '',true);
	wp_enqueue_script('bootstrapjs',
		'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js',
		[],'4.6.1',true);
	wp_enqueue_script("mainjs",
		get_template_directory_uri() . '/js/main.js',
		[], '1.0', true);
}

// hook d'ajout des scripts
add_action('wp_enqueue_scripts','monsupertheme_scripts');