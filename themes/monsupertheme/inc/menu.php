<?php

// function de zone de menu
function register_menu_supertheme()
{
	register_nav_menus(
		array(
			'menu-sup' => __("Menu superieur"),
		)
	);
}
// ajout de ma fonction dans le hook d'init
add_action('init','register_menu_supertheme');


// classe de reconstruction du menu
class projetpro_walker extends Walker_Nav_Menu
{
	function start_el( &$output, $data_object, $depth = 0, $args = null, $current_object_id = 0 ) {
		$menu_item = $data_object;

		// echo print_r($menu_item, true);
		$title = $menu_item->title;
		$description = $menu_item->description;
		$permalink = $menu_item->url;
		// debut de construction de notre item de menu
		$output .= '<li class="nav-item">';

		if( $permalink && $permalink != '#'){
			$output .= '<a href="' . $permalink . '" class="nav-link">';
		} else {
			$output .= '<span>';
		}

		$output .= $title;

		if( $description != '' && $depth == 0) {
			$output .= "<small class='description'>" . $description . "</small>";
		}

		if( $permalink && $permalink != '#'){
			$output .= '</a>';
		} else {
			$output .= '</span>';
		}

	}
}