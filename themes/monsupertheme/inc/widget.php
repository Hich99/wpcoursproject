<?php
// déclaration de la zone de widget
function register_widget_supertheme()
{
	register_sidebar(
		[
			"name" => "Ma sidebar",
			"description" => __("ceci est ma sidebar"),
			"id" => "masidebar",
			"before_widget" =>"<div class='border border-info mb-3 rounded p-2'>",
			"after_widget" => "</div>",
			"before_title" => "<h4 class='widget-title'>",
			"after_title" => "</h4>"
		]
	);
	register_sidebar(
		[
			"name" => "Ma Footer",
			"description" => __("ceci est ma zone footer "),
			"id" => "mafooter",
			"before_widget" =>"<div class='border border-info mb-3 rounded p-2'>",
			"after_widget" => "</div>",
			"before_title" => "<h4 class='widget-title'>",
			"after_title" => "</h4>"
		]
	);
    register_sidebar(
		[
			"name" => "central widget",
			"description" => __("ceci est ma zone central "),
			"id" => "macentral",
			"before_widget" =>"<div class='border border-info mb-3 rounded p-2'>",
			"after_widget" => "</div>",
			"before_title" => "<h4 class='widget-title'>",
			"after_title" => "</h4>"
		]
	);
//    register_widget('WP_Widget_First');
//    register_widget('WP_Widget_Unsplash');
}
add_action('widgets_init', 'register_widget_supertheme');

