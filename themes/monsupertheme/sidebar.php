<section
    style="background-color: darkblue; color: white;"
    class="col-sm-3 offset-sm-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
        <?php if (is_active_sidebar('masidebar')) :
            dynamic_sidebar('masidebar');
        endif;
        ?>
        <?php if ( ! is_single() ) : ?>
        <h4>A propos</h4>
        <!-- récupère la description de l'auteur -->
        <p><?php the_author_meta('description'); ?></p>

        <h4>Archives</h4>
        <ol class="list-unstyled">
            <?php wp_get_archives('type=monthly');?>
        </ol>
        <?php else:
            $current_id = get_the_ID();

echo $current_id;
            ?>
        <h4>Autres Articles</h4>

        <ol class="list-unstyled">
            <?php
                $author_posts = new WP_Query( array( 'author' => get_the_author_meta( 'ID' ) ) );
                while ( $author_posts->have_posts() ) : $author_posts->the_post();
                    if( get_the_ID() !== $current_id ):
                ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php
                endif;
                endwhile;
            ?>
        </ol>
        <?php endif; ?>
    </div>
</section>