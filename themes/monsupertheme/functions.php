<?php
// fichier de config
//var_dump(get_template_directory_uri(). "/inc/config.php");
require_once get_template_directory(). "/inc/config.php";

// fichier menu
require_once get_template_directory(). "/inc/menu.php";

//fichier widget
require_once get_template_directory(). "/inc/widget.php";

// fichier script
require_once get_template_directory(). "/inc/scripts.php";
