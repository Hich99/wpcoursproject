<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mon super theme</title>
    <?php wp_head(); ?>
<!--    <link rel="stylesheet" -->
<!--        href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"-->
<!--          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"-->
<!--          crossorigin="anonymous">-->
</head>
<body class="container">
    <?php if( get_header_image() ) :?>
        <div id="site-header">
            <figure>
                <img src="<?php header_image(); ?>"
                     width="100%"
                     height="<?php echo get_custom_header()->height; ?>"
                     alt="image entête"/>
            </figure
        </div>
    <?php endif; ?>
    <header class="jumbotron" style="color:#fff; background-color:#2bcaff !important;">
        <h2 class="blog-title text-right">
            <a href="<?php echo get_bloginfo('wpurl'); ?>">
                <?php echo get_bloginfo('name'); ?>
            </a>
        </h2>
        <p class="text-right"><?php echo get_bloginfo('description'); ?></p>
        <?php wp_nav_menu(
//            array(
//                'theme_location' => 'menu_sup',
//                'container' => '',
//                'menu_class' => 'navbar-nav mr-auto',
//                'menu_id' => '',
//                'walker' => new projetpro_walker()
//            )
        ); ?>
    </header>