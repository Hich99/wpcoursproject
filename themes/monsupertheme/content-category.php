<header class="entry-header">
	<h3>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php the_title(); ?>
        </a>
        <?php if ('post' == get_post_type() ) : ?>
            <div class="blog-postmeta">
                <div class="post-date">
                    <?php echo get_the_date(); ?> <!-- post date -->
                </div>
            </div>
        <?php endif; ?>
    </h3>
</header>
<div class="entry-summary">
    <?php the_excerpt(); ?>
    <a href="<?php the_permalink(); ?>">(<?php esc_html_e('lire la suite &rarr;'); ?>)</a>
</div>