<h4 class="blog-post-title text-primary"><?php the_title(); ?></h4>
<em class="blog-post-meta text-right">
    <?php the_date(); ?> par <a href="#"><?php the_author(); ?></a>
</em>
<p><?php the_content(); ?></p>