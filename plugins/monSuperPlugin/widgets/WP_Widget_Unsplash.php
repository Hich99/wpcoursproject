<?php

class WP_Widget_Unsplash extends WP_Widget
{
	public function __construct() {
		$widget_options = [
			'className' => 'WP_Widget_Unsplash',
			'description' => __("photos aléatoires"),
			'customize_selective_refresh' => true,
		];
		parent::__construct( 'randomPhoto', "Unsplash widget",
			$widget_options );
	}

	public function form( $instance ) {
		$instance = wp_parse_args((array) $instance, ['apikey'=>'', 'count'=> '', 'choix'=>'']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('apikey');?>">
				<?php _e("Clé d'acces API unsplash"); ?>
			</label>
			<input type="text"
			       id="<?php echo $this->get_field_id('apikey');?>"
			       name="<?php echo $this->get_field_name('apikey');?>"
			       value="<?php echo $instance['apikey']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('count');?>">
				<?php _e("nombre d'image"); ?>
			</label>
			<input type="text"
			       id="<?php echo $this->get_field_id('count');?>"
			       name="<?php echo $this->get_field_name('count');?>"
			       value="<?php echo $instance['count']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('choix');?>">
				<?php _e("Thème des images"); ?>
			</label>
			<input type="text"
			       id="<?php echo $this->get_field_id('choix');?>"
			       name="<?php echo $this->get_field_name('choix');?>"
			       value="<?php echo $instance['choix']; ?>">
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['apikey'] = sanitize_text_field( $new_instance['apikey'] );
		$instance['count'] = sanitize_text_field( $new_instance['count'] );
		$instance['choix'] = sanitize_text_field( $new_instance['choix'] );
		return $instance;
	}

	public function widget( $args, $instance ) {

		$title = "Photo unsplash";

		// défini l'url d'appel de l'api
		$url = "https://api.unsplash.com/photos/random/?client_id=".
		       $instance['apikey'] ."&count=" . intval($instance['count']) .
		       "&query=" . $instance['choix'];

		// appel de l'api
		$request = wp_remote_get($url);

		if( is_wp_error( $request ) ){
			return false;
		}

		// récupération du corps de la requete (json)
		$body = wp_remote_retrieve_body( $request );

		//décoder le json
		$datas = json_decode($body, true);

		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title'];
		echo "<div>";
		if( !empty( $datas ) ) {
			foreach ($datas as $data) {
				echo "<img src='" . $data['urls']['thumb'] .
				     "' alt='" . $data['alt_description'] . "' />";
			}
		}
		echo "</div>";
		echo $args['after_widget'];
	}
}