<?php
// mon premier
class WP_Widget_First extends WP_Widget
{
	private $eleves = [
		"choisir un élève",
		"William",
		"Mathis",
		"Alexis",
		"Xavier",
		"Kevin",
		"lucas",
	];

	// constructeur de notre widget
	public function __construct() {
		$widget_opt = [
			'className' => 'WP_Widget_First',
			'description' => __("Choix de l'élève de la semaine"),
			'customize_selective_refresh' => true
		];
		parent::__construct('first_widget', __('Mon premier widget'), $widget_opt);
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, ['eleve' => ''] );
		?>
		<label for="<?php echo $this->get_field_id('eleve'); ?>">
			<?php _e("Elève : "); ?>
		</label>
		<select name="<?php echo $this->get_field_name('eleve'); ?>"
		        id="<?php echo $this->get_field_id('eleve'); ?>"
		        class="widefat">
			<?php foreach ($this->eleves as $key => $eleve){
				$selected = ($key == $instance['eleve']) ? ' selected ' : '';
				?>
				<option value="<?php echo $key; ?>" <?php echo $selected; ?> >
					<?php echo $eleve; ?>
				</option>
			<?php }?>
		</select>
		<?php

	}

	/*
	 * function de mise à jour de la donnée
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['eleve'] = sanitize_text_field( $new_instance['eleve'] );
		return $instance;
	}


	public function widget( $args, $instance ) {
		$title = "The best guy of the week !";
		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title'];
		echo "<div>" . $this->eleves[ intval( $instance['eleve'] ) ] ."</div>";
		echo $args['after_widget'];
	}
}