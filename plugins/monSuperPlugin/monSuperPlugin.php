<?php
/*
 * Plugin Name: Projet Pro plugin
 * Description: gestion de produits de vienoiseries
 * Author: Projet Pro
 * Version: 1.0
 */
require_once plugin_dir_path(__FILE__) . "/widgets/WP_Widget_First.php";
require_once plugin_dir_path(__FILE__) . "/widgets/WP_Widget_Unsplash.php";
require_once plugin_dir_path(__FILE__) . "/data/Dal.php";

class monSuperPlugin
{

	public function __construct() {
		new Dal();
		// event activation
		register_activation_hook(__FILE__, array('Dal', 'install'));

//		register_deactivation_hook(__FILE__, array('Dal', 'uninstall'));

		register_uninstall_hook(__FILE__, array('Dal', 'uninstall'));

		// instenciation des widgets
		add_action('widgets_init',function (){
			register_widget('WP_Widget_Unsplash');
			register_widget('WP_Widget_First');
		});

		add_action('admin_menu', array($this, 'add_menu_back'));

	}

	public function add_menu_back() {
		$menu = add_menu_page("Les patisseries",
			"Les patisseries","manage_options",
			"patisserie",array($this, "patisseriePage"),
			"dashicons-buddicons-community", 45);
		add_submenu_page('patisserie', 'ajouter une patisserie',
			'Ajouter',"manage_options","addPatisserie",
			array($this, 'patisseriePage'));
	}

	public function patisseriePage() {

		$dal = new Dal();
		$page = $_REQUEST['page'];
		$html = "";
		$html .= "<div class='wrap'>";
		$html .= "<h3>Les patisseries de la boulangère</h3>";
		$html .= "<h5>nous sommes sur la page $page</h5>";

		switch ($page){
			case 'patisserie': // affichage du tableau
				if( isset($_POST['nom']) && ! empty($_POST['nom']) ){ // tester si des champs sont postés
					$dal->savePatisserie();
				}
				$html .= "<table class='widefat fixed' cellspacing='0'>";
				$html .= "<tr>".
				         "<th class='manage-column column-columnname'>ID</th>".
				         "<th class='manage-column column-columnname'>Nom</th>".
				         "<th class='manage-column column-columnname'>Type de pate</th>".
				         "<th class='manage-column column-columnname'>Ingredients</th>".
				         "</tr>";
				foreach ($dal->findAll() as $line){
					$html .= "<tr>" .
					         "<td>" . $line['id'] ."</td>".
					         "<td>" . $line['nom'] ."</td>".
					         "<td>" . $line['type_pate'] ."</td>".
					         "<td>" . $line['ingredient'] ."</td>".
					         "</tr>";
				}

				$html .= "</table>";
				break;
			case 'addPatisserie': // affichage du formulaire
					$html .= "<form method='post'>".
					         "<div class=''>".
					         "<label for='nom'>Nom de la patisserie</label>".
					         "<input name='nom' id='nom' type='text'/>".
					         "</div>".
					         "<div class=''>".
					         "<label for='type_pate'>Type de pate</label>".
					         "<input name='type_pate' id='type_pate' type='text'/>".
					         "</div>".
					         "<div>".
					         "<label for='ingredient'>ingredient</label>".
					         "<textarea name='ingredient' rows='5' cols='40'></textarea>".
					         "</div>".
					         "<input type='hidden' name='page' value='patisserie' /> ".
					         "<input type='submit' value='Enregistrer' /> ".
					         "</form>";
				break;
		}


		$html .= "</div>";

		echo $html;
	}
}

new  monSuperPlugin();