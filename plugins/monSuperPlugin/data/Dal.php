<?php

class Dal {

	private $myWpdb;

	public function __construct() {
		global $wpdb;
		$this->myWpdb = $wpdb;
	}

	/**
	 * creation de la table produit à l'activation du plugin
	 */
	public static function install()
	{
		global $wpdb;
		// table wp_supertheme_patisserie
		$query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}supertheme_patisserie (" .
		         "id INT(11) AUTO_INCREMENT PRIMARY KEY,".
		         "nom VARCHAR(150) NOT NULL,".
		         "type_pate VARCHAR(100) NOT NULL,".
		         "ingredient TEXT NOT NULL );";
		$wpdb->query($query);

	}

	/**
	 * suppression de la table
	 */
	public static function uninstall() {
		global $wpdb;
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}supertheme_patisserie;");
	}

	public function findAll() {
		$res = $this->myWpdb->get_results("select  * from {$this->myWpdb->prefix}supertheme_patisserie;",
			ARRAY_A);
		return $res;
	}

	/**
	 * sauvegarde de la patisserie
	 */
	public function savePatisserie() {

		$test = true;
		$array_var = ['nom', 'type_pate', 'ingredient']; // variable mysql
		$insert = [];

		foreach ($array_var as $key){ // parcours des variables mysql
			if( !array_key_exists($key, $_POST) || empty($_POST[$key]) ){ // la variable n'existe pas ou elle est vide
				$test = false;
			} else {
				$insert[$key] = $_POST[$key];
			}
		}

		if($test){ // si test true
			$row = $this->myWpdb->get_row("select *  from".
               " {$this->myWpdb->prefix}supertheme_patisserie where nom='" . $insert['nom'] . "';");
			if( is_null($row) ){
				$this->myWpdb->insert("{$this->myWpdb->prefix}supertheme_patisserie", $insert);
			} else {
				echo "cette patisserie existe déjà !";
			}
		} else {
			echo " il y a un problème avec les données";
		}


	}

}